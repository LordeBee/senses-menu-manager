const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');

function updateItemPrice(menu, categoryId, itemName, newPrice) {
    const category = menu.find(category => category.id === categoryId);

    if (category) {
        const item = category.list.find(menuItem => menuItem.name === itemName);

        if (item) {
            item.price = newPrice;
            return {
                success: true,
                message: `Updated price of ${itemName} to ${newPrice}`
            }
        } else {
            return {
                success: false,
                message: `Item ${itemName} not found in category ${categoryId}`
            }
        }
    } else {
        return {
            success: false,
            message: `Category with id ${categoryId} not found`
        }
    }
}

const app = express();
const port = 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Serve HTML file
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

app.get('/menu', (req, res) => {
    res.sendFile(__dirname + '/menu.html');
});

app.get('/menu-data', (req, res) => {
    res.sendFile(__dirname + '/menu.json');
});

// API endpoint to handle updates
app.post('/update-item', (req, res) => {
    console.log(req.body)
    const updatedItem = req.body;

    // Read the existing JSON file
    const jsonData = JSON.parse(fs.readFileSync('menu.json', 'utf-8'));



    const update = updateItemPrice(jsonData, 1, "MOZARELLA STICKS", 80);
    if (update.success) {
        console.log(update.message)
        fs.writeFileSync('menu.json', JSON.stringify(jsonData, null, 2), 'utf-8');
    }
    res.send(update.message);
});

app.listen(port, () => {
    console.log(`Server is running at http://localhost:${port}`);
});
